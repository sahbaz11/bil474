<?php

	session_start();
	
	// Baglanti kur
	$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
		if($_FILES)	{
			$tip=$_FILES['dosya']['type']; // yüklenecek dosyanın tipini alıyoruz		
			$dboyut	= $_FILES["dosya"]["size"];
			$kaynak = $_FILES["dosya"]["tmp_name"];
			$bugun = date("Ymdhis"); 
			$dosya_name=$_FILES['dosya']['name'];
			$uzanti	= substr($dosya_name, -4);
			$hedef ="image/user";
			$yeniResimAdi = $bugun.$uzanti;
			
			//burada tip kontrolü yapıyoruz güvenlik gereği sadece belirli dosyaların yüklenmesine izin veriyoruz
			if(!($tip=="image/x-png" || $tip=="image/png" || $tip=="image/pjpeg" || $tip=="image/gif" || $tip=="image/jpeg" || $tip=="image/jpg")) {
			
				$_SESSION['fotoMesaj'] ="Uzantı uymamaktadır sadece <b> 'png, jpeg, gif</b> dosya türlerini yükleyebilirsiniz";		
				header('Location: modeladd.php');
			}
			else if($dboyut > "2097152") {		
				$_SESSION['fotoMesaj'] ="HATA! Yüklemeye calıştığınız dosya büyük boyutta. Yükyeleyebileceğiniz boyut en fazla 2 Mbyte olmalıdır!";
			}
			else {
				$yukle = move_uploaded_file($kaynak, $hedef.'/'.$yeniResimAdi);
			
				if($yukle){
					$sorgu = "UPDATE user SET picturepath = '" . $yeniResimAdi . "' WHERE id = ". $_SESSION['activeUser'];
					$result = $conn->query($sorgu);
				}
				else {
					$_SESSION['fotoMesaj'] ="Dosya yüklemesinde bir hata var. Hata Kodu ".$_FILES['dosya']['error'];
				}
			}
		 }
		 else {		
			$_SESSION['fotoMesaj'] ="Dosya yüklemesinde bir hata var. Hata Kodu ".$_FILES['dosya']['error']; 
		}
		
	$conn->close();
	header('Location: userpicture.php');
	unset($_SESSION['updatecar']);
?>