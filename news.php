<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset= windows-1254" />
<meta name="Author" content=" Alper SAHBAZ" />
<link rel="stylesheet" href="style/otokritik.css" />
<link rel="shortcut icon" href="image/araba.png" />
<title> Haberler </title>
</head>

<body>
<div class="container">
	<div id="header">
		<?php 
			session_start();
			$_SESSION['lastPage'] = "news.php?new=" . $_GET['new'];
			include('header.php');
		?>
	</div>

	<div class="dis_bolme">
		<div class="yuzdeLeft">
			<?php include('solpen.php'); ?>
		</div>
		
        <div class="yuzdeMid">
        	<?php
				$newsid = $_GET['new'];
				$_SESSION['secili_haber'] = $newsid;
				
				// ortadaki alan
				include('newsmid.php');
			?>
        </div>
		
        <div class="yuzdeRight">
			<?php include('sagpen.php'); ?>
		</div>

	</div>
    
    <div class="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>
</body>
</html>