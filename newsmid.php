
	<div class = "newswrapper">
		<br /><br />
		<a href = "news.php?new=0" class = "newsmaintitle"> <u> HABERLER </u> </a>
		<br /><br /><br /><br />
		<?php if(isset($_SESSION['activeUser'])) { ?>
		<a href = "news.php?new=-1" class = "newsaddtitle"> <u> + Haber Ekle </u> </a>
		<br />
	<?php
		}
		// Baglanti kur
		$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		// tum haberleri listele
		if($_SESSION['secili_haber'] == 0) {
			$sorgu = "SELECT * FROM news ORDER BY id DESC";
			$result = $conn->query($sorgu);
			
			if ($result->num_rows > 0) {
			?>
				<div class = "newsalldiv">
				<div class = "newsempty"> </div>
					<table class = "newsalltable">
			<?php
				while($row = $result->fetch_assoc()) {
					$icsorgu = "SELECT username FROM user WHERE id = " . $row["userid"];
					$icresult = $conn->query($icsorgu);
					
					if ($icresult->num_rows > 0) {
						$icrow = $icresult->fetch_assoc();
						$newsusername = $icrow["username"];
					}
			?>
					<tr class = "newstablerow">
						<td class = "newstablecellimg">
							<img src = "image/news.png" class = "newsimg"/>
						</td>
						<td class = "newstablecontent">
						<a href = "news.php?new=<?php echo $row["id"];?>">
							<div class = "newstabletitle">
								&nbsp;&nbsp;&nbsp;<?php echo $row["title"]; ?>
							</div>
							<div class = "newstablesummary">
								<?php echo substr($row["text"], 0, 140) . "..."; ?>
							</div>
							<div class = "newstablewriter">
								<?php echo "yazar : " . $newsusername; ?>
							</div>
						</a>
						</td>
					</tr>
					<tr class = "newstablerowempty"> </tr>
			<?php
				}
			?>
					</table>
				</div>
			<?php
			} else {
				// haber yok
				?>
					<div class = "newsyok">
						Sistemde kayıtlı hiçbir haber yok!
					</div>
				<?php
			}
		}
		// haber ekleme sayfasi
		else if($_SESSION['secili_haber'] == -1) {
			if(isset($_SESSION['activeUser'])) {
				?>
					<div class = "newnew">
						<form action="newsinput.php" method="post">
							<input class = "newsinputtitle" type = "text" name = "haberbaslik" placeholder = "Başlık" />
							<div class = "newsinputempty"> </div>
							<textarea  class = "newsinputtext" type = "text" name = "habericerik"></textarea>
							<div class = "btnnewsdiv">
								<input type="image" name="submit" src="image/olustur.png" class = "btnnews" />
							</div>
							<div class = "newsinputempty"> </div>
						</form>
					</div>
				<?php
			} else {
				echo "Bu sayfayı kullanma yetkiniz yok. Lütfen giriş yapın!";
			}
		}
		// spesifik bir haber ac
		else {
			$sorgu = "SELECT * FROM news WHERE id = " . $_SESSION['secili_haber'];
			$result = $conn->query($sorgu);
			
			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$titleofnew = $row["title"];
				$textofnew = $row["text"];
				$timeofnew = $row["time"];
				
				$icsorgu = "SELECT username FROM user WHERE id = " . $row["userid"];
				$icresult = $conn->query($icsorgu);
				
				if ($icresult->num_rows > 0) {
					$icrow = $icresult->fetch_assoc();
					$userofnew = $icrow["username"];
				}
				
			?>
				<div class = "newstitlediv">
					-<?php echo $titleofnew;?>-
				</div>
				
				<div class = "newstimediv">
					<u><?php 
						$loc_tr = setlocale(LC_ALL, 'tr_TR.UTF-8', 'tr_TR', 'tr', 'turkish');
						$date = date_create($timeofnew);
						echo date_format($date, 'j F Y , l');?></u>
				</div>
				
				<div class = "newstextdiv">
					<?php echo $textofnew;?>
				</div>
				
				<div class = "newsuser">
					Yazar : <b><i><?php echo $userofnew;?></i></b>
				</div>
				
				<?php if(isset($_SESSION['activeUser'])) { ?>
					<div class = "newscommentsdiv">
						<div class = "newscommenttitle"> Yorum Yap : </div>
						<form action="newscomment.php" method="post">
							<textarea  class = "commentinputtext" type = "text" name = "commenticerik"></textarea>
							<div class = "btnnewsdiv">
								<input type="image" name="submit" src="image/yorumyap.png" class = "btncomment" />
							</div>
						</form>
					</div>
				<?php } else {?>
					<div class = "newscommentsdiv">
						Yorum yapabilmek i&#231in l&#252tfen giri&#351 yap&#305n!
					</div>
				<?php } 
					// daha once yapilan yorumlar
					$sorgu = "SELECT * FROM newscomment WHERE newsid = " . $_SESSION['secili_haber'] . " ORDER BY id DESC";
					$result = $conn->query($sorgu);
					
					echo "<div class = \"newscommentalldiv\">";
					
					if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
							$icsorgu = "SELECT username FROM user WHERE id = " . $row["userid"];
							$icresult = $conn->query($icsorgu);
							
							if ($icresult->num_rows > 0) {
								$icrow = $icresult->fetch_assoc();
								$commentusername = $icrow["username"];
							} else {
								$commentusername = "Anonim";
							}
					?>
							<div class = "commentcontent">
								<?php echo $row["comment"];?>
							</div>
							<div class = "commentbot">
								<div class = "commentdate">
									<?php $date = date_create($row["time"]);
										echo date_format($date, 'j F Y');?>
								</div>
								<div class = "commentuser">
									<?php echo $commentusername;?>									
								</div>
							</div>
							
					<?php
						}
					} else {
						echo "Bu haber hakk&#305nda hi&#231bir yorum yap&#305lmam&#305&#351!";
					}
					
					echo "</div>";
			} else {
				echo "Bir hata oluştu!";
			}
		}
		
		$conn->close();
	?>
	</div>