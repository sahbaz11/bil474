<?php

	session_start();
	
	unset($_SESSION['activeUser']);
	unset($_SESSION['activeUserRole']);
	unset($_SESSION['activeUserName']);
	
	$git = "Location: " . $_SESSION['lastPage'];
	header($git);
?>