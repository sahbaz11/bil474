<?php
	session_start();
	
	/*
		1 - 'Kullanici adi en az 3 karakter olmali!'
		2 - 'Parola en az 5 karakter olmali!'
		3 - 'Parola en az 5 karakter olmali!'
		4 - 'Ad en az 3 karakter olmali!'
		5 - 'Soyad en az 2 karakter olmali!'
		6 - 'Zorunlu alan: E-posta!'
		7 - 'Bu üye adı sistemimizde mevcuttur!'
		8 - 'Bu e-posta adresi sistemimizde mevcuttur!'
		9 - 'Parolalar Birbirini tutmuyor!'
	*/
	
	$checkusername = $_POST["uyeadi"];
	// Baglanti kur
	$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	$result = $conn->query("SELECT * FROM user WHERE username ='$checkusername' ");
	if (($result->num_rows > 0) || (strlen($checkusername) < 3)) {
		$_SESSION['checkname'] = 1;
	} else {
		$_SESSION['checkname'] = 0;
	}
	$conn->close();

	$kullaniciAdi = $_POST["uyeadi"];
	if(!empty($kullaniciAdi)) {
		$_SESSION['kaydolusername'] = $kullaniciAdi;
	}
	
	$sifre = $_POST["sifre"];
	if(!empty($sifre)) {
		$_SESSION['kaydol1pass'] = $sifre;
	}
	
	$sifre2 = $_POST["sifre2"];
	if(!empty($sifre2)) {
		$_SESSION['kaydol2pass'] = $sifre2;
	}
	
	$adi = $_POST["adi"];
	if(!empty($adi)) {
		$_SESSION['kaydolname'] = $adi;
	}
	
	$soyadi = $_POST["soyadi"];
	if(!empty($soyadi)) {
		$_SESSION['kaydolsurname'] = $soyadi;
	}
	
	$mail = $_POST["mail"];
	if(!empty($mail)) {
		$_SESSION['kaydolmail'] = $mail;
	}
	
	if((strlen($kullaniciAdi) < 3)) {
		$_SESSION['kaydolmesaj'] = 1;
	} else if((strlen($sifre) < 5)) {
		$_SESSION['kaydolmesaj'] = 2;
	} else if((strlen($sifre2) < 5)) {
		$_SESSION['kaydolmesaj'] = 3;
	} else if((strlen($adi) < 3)) {
		$_SESSION['kaydolmesaj'] = 4;
	} else if((strlen($soyadi) < 2)) {
		$_SESSION['kaydolmesaj'] = 5;
	} else if((strlen($mail) < 1)) {
		$_SESSION['kaydolmesaj'] = 6;
	} else {
		// Baglanti kur
		$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		
		$result = $conn->query("SELECT * FROM user WHERE username ='$kullaniciAdi' ");
		if ($result->num_rows > 0) {
			$_SESSION['kaydolmesaj'] = 7;
		} else {
			$result = $conn->query("SELECT * FROM user WHERE mail ='$mail' ");
			if ($result->num_rows > 0) {
				$_SESSION['kaydolmesaj'] = 8;
			} else {
		
				if(!strcmp($sifre,$sifre2))	{
					$result = $conn->query("INSERT INTO user (username,name,surname,mail,pass) VALUES ('$kullaniciAdi','$adi','$soyadi','$mail','$sifre')");
					$_SESSION['kaydolbasarili'] = 1;
				}
				else {
					$_SESSION['kaydolmesaj'] = 9;
				}
			}
		}
	}
	
	$conn->close();
	header('Location: kaydol.php');
?>