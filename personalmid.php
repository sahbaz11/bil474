
	<?php
		// Baglanti kur
		$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$sorgu = "SELECT name, surname, used1carid, used2carid, pass, mail, picturepath, role FROM user WHERE id = ". $_SESSION['activeUser'];
		$result = $conn->query($sorgu);

		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$userName = $_SESSION['activeUserName'];
			$name = $row["name"];
			$surname = $row["surname"];
			$mail = $row["mail"];
			$used1carid = $row["used1carid"];
			$used2carid = $row["used2carid"];
			$picturepath = $row["picturepath"];
			$role = $row["role"];
		}
		
		if(!empty($used1carid)) {	
			$sorgu = "SELECT * FROM usercars WHERE id = ". $used1carid;
			$result = $conn->query($sorgu);

			if ($result->num_rows > 0) {
				$row = $result->fetch_assoc();
				$imagepath = $row["imagepath"];
				$otoname = $row["name"];
			}
		}
		
		$conn->close();
	?>
	
	
	<form action="guncelle.php" method="post">
	<div class = "userback">
		<div class = "usertop">
			<div class = "userpict">
				<div class = "userphotodiv">
					<img src = "image/user/<?php 
					if(empty($picturepath)) {
						echo "default.png";
					} else {
						echo $picturepath;
					}?>" class = "userphoto"/>
						
				</div>
				<div class = "userchangephotodiv">
					<a href ="userpicture.php" class = "userchangephoto"> Profil Fotografini Degistir </a>
				</div>
			</div>
			
			<div class = "userinftop">
				<table class = "userinfotable">
					<tr>
						<td class = "usertableusername" colspan = "6">
							<?php echo $userName; ?>
						</td>
					</tr>
					<tr>
						<td class = "usertablecell">
							Ad
						</td>
						
						<td class = "usertablecell">
							:
						</td>
						
						<td class = "usertablecellcontent">
							<?php
								echo $name;
							?>
						</td>
					</tr>
					<tr>
						<td class = "usertablecell">
							Soyad
						</td>
						
						<td class = "usertablecell">
							:
						</td>
						
						<td class = "usertablecellcontent">
							<?php
								echo $surname
							?>
						</td>
					</tr>
					<tr>
						<td class = "usertablecell">
							E-Posta
						</td>
						
						<td class = "usertablecell">
							:
						</td>
						
						<td class = "usertablecellcontent" colspan = "2">
							<?php
								echo $mail;
							?>
						</td>
					</tr>
					<tr>
						<td class = "usertablecell">
							Rol
						</td>
						
						<td class = "usertablecell">
							:
						</td>
						
						<td class = "usertablecellcontent" colspan = "2">
							<?php
								if($role == 0) {
									echo "Standart Kullanici";
								} else if($role == 1) {
									echo "Yonetici";
								} else {
									echo "Hata";
								}
							?>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class = "usermid">
			<div class = "userempty"></div>
			<div class = "usercardiv">
				<center><u> &nbsp;&nbsp;Arabalar&#305: </u></center>
				<?php 
					if(!empty($used1carid)) { ?>
						<div class = "userusedcarpictdiv">
							<img src = "image/usercar/<?php echo $imagepath; ?>" class = "userusedcarpict">
						</div>
						<div class = "userusedcarname">
							<a href = "usercar.php?oto=<?php echo $used1carid; ?>"> <?php echo $otoname ?></a>
						</div>
				<?php
					} else {?>
						<div class = "userusedcarpictdiv">
							<img src = "image/usercar/default.png" class = "userusedcarpict">
						</div>
						<div class = "userusedcarname">
							<a href = ""> Araba Yok </a>
						</div>
				<?php
					}
				?>
			</div>
			<div class = "userbottomortala"> </div>
			<div class = "userpasschange">
				<table class = "userpasschangetable">
					<tr>
						<td colspan = "2" class = "userpasschangetitle">
							Parolanizi Degistirin
						</td>
					</tr>
					<tr><td><br/></td></tr>
					<tr>
						<td>
							Mevcut Parola
						</td>
						<td>
							<input id = "mevcutpassword" class = "degisinput" type = "password" name = "mevcutpassword"/>
						</td>
					</tr>
					<tr>
						<td>
							Yeni Parola
						</td>
						<td>
							<input id = "yeni1password" class = "degisinput" type = "password" name = "yeni1password"/>
						</td>
					</tr>
					<tr>
						<td>
							Yeni Parola(2)
						</td>
						<td>
							<input id = "yeni2password" class = "degisinput" type = "password" name = "yeni2password"/>
						</td>
					</tr>
					<tr><td class = "<?php 
							// parola degistirildiyse
							if(isset($_SESSION['parolaDegisimi'])) {
								echo "degisuyaripos";
							} else {
								echo "degisuyari";
							}
						?>" colspan = "2">
						<div id="guncelleuyaridiv" style="display:block">
						<?php
							// parola degistirildiyse
							if(isset($_SESSION['parolaDegisimi'])) {
								echo $_SESSION['parolaDegisimi'];
								unset($_SESSION['parolaDegisimi']);
							} else {
								if(isset($_SESSION['guncelleHata'])) {
									echo $_SESSION['guncelleHata'];
								}
							}
						?>
						</div>
						
						<script type="text/javascript">
							window.setTimeout(uyarikaybol, 5000);
							function uyarikaybol(){
								document.getElementById("guncelleuyaridiv").style.display="none";
							}
						</script>
				</td></tr>
					<tr>
						<td>
						</td>
						<td class = "rightalign">
							<input type="image" name="submit" src="image/degis.png" class = "degisbuton">
						</td>
					</tr>
				</table>
			</div>
			<div class = "userempty"></div>
		</div>
	</div>
	</form>