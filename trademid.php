
	<?php
		// Baglanti kur
		$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
		
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$sorgu = "SELECT name, countryid, summary FROM trade WHERE id = ". $_SESSION['secili_marka'];
		$result = $conn->query($sorgu);

		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$tradename = $row["name"];
			$tradenamebuyuk = strtoupper($tradename);
			$tradecountryid = $row["countryid"];
			$tradesummary = $row["summary"];
		}
		$sorgu = "SELECT name FROM country WHERE id = ". $tradecountryid;
		$result = $conn->query($sorgu);
		
		if ($result->num_rows > 0) {
			$row = $result->fetch_assoc();
			$tradecountryname = $row["name"];
		}
		
		$trademodels  = array();
		$trademodelids = array();
		
		$sorgu = "SELECT id, name FROM model WHERE tradeid = ". $_SESSION['secili_marka'];
		$result = $conn->query($sorgu);
		
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$trademodelids[] = $row["id"];
				$trademodels[] = $row["name"];
    		}
		}
		
		// rating bilgileri
		$userrating = 0;
		$totalrating = 0;
		$ratingcount = 0;
		
		$sorgu = "SELECT * FROM traderating WHERE tradeid = ". $_SESSION['secili_marka'];
		$result = $conn->query($sorgu);

        if ($result->num_rows > 0) {
   			while($row = $result->fetch_assoc()) {
				$ratingcount++;
				$totalrating = $totalrating + $row["rating"];
				if(isset($_SESSION['activeUser'])) {
					if($row["userid"] == $_SESSION['activeUser']) {
						$userrating = $row["rating"];
					}
				}
    		}
		}
		
		$conn->close();
	?>
    <div class="tradeheadback">
    <div class="tradehead">
        <div class="tradeheadleft">
            <?php
                echo "<img src=\"image/trade" . $_SESSION['secili_marka'] . "/logobuyuk.png\" height=\"85\"/>";
            ?>
        </div>
        
        <div class="tradeheadmid">
            <?php
                echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $tradenamebuyuk ;
            ?>
        </div>
        
        <div class="tradeheadright">
            <div class="tradeheadrighttop">
                <?php
                echo "<img src=\"image/flags/" . $tradecountryid . ".png\" height=\"60\"/>";
                ?>
            </div>
            
            <div class="tradeheadrightbot">
                <?php
                    echo "&nbsp;&nbsp;" . $tradecountryname;
                ?>
            </div>
        </div>
    </div>
    </div>
    <div class="tradesummary">
    	<p>
        	&nbsp;&nbsp;&nbsp;&nbsp;
        	<?php
				echo "-" . $tradesummary;
			?>
        </p>
    </div>
    
	<div class = "tradebot">
		<div class="trademodels">
			<p class="boldparagraph">
				<br/>
				<br/>
				Modeller : 
			</p>
			<p>
				<ul>
					<?php
						$boyut = count($trademodels);
						for ($i = 0; $i < $boyut; $i++) {
							echo "<li> &nbsp;&nbsp;&nbsp;&nbsp; <a href = \"models.php?model=" . $trademodelids[$i] . "\">" . $trademodels[$i] . "</a>";
							if((isset($_SESSION['activeUserRole'])) && ($_SESSION['activeUserRole'] == 1)) {
								echo "&nbsp;&nbsp;&nbsp;&nbsp; <a href = \"modelupdate.php?model=" . $trademodelids[$i] . "\">[düzenle]</a>";
							}
							echo"</li>";
						}
						
						// kullanici yonetici yetkisindeyse
						if((isset($_SESSION['activeUserRole'])) && ($_SESSION['activeUserRole'] == 1)) {
							echo "<br /> <li> &nbsp;&nbsp;&nbsp;&nbsp; <a href = \"modeladd.php\"><i> + yeni model ekle </i></a></li>";
						}
					?>
				</ul>
			</p>
		</div>
		
		<div class = "traderatingdiv">
			<div class = "tradestarback">
				<div class = "tradestarempty"> </div>
				<div class = "tradestarbackwrapper">
					<div class = "tradestarpercenttop" style = "height : <?php
						if($ratingcount == 0) {
							echo "120";
						} else {
							echo 120 - (12 * ($totalrating / $ratingcount));
						}
					?>px;">
					
					</div>
					<div class = "tradestarpercentbot" style = "height : <?php
						if($ratingcount == 0) {
							echo "0";
						} else {
							echo 12 * ($totalrating / $ratingcount);
						}
					?>px;">
					
					</div>
				</div>
			</div>
			<div class = "traderating">
				<div class = "tradestarempty"> </div>
				<div class = "tradestarwrapper">
					<img src = "image/starbos.png" class = "tradestarpict" />
				</div>
			</div>
			<div class = "traderatinginf">
				<?php
					echo "<b>";
					if($ratingcount == 0) {
						echo "Bu marka için hiç oy kullanılmamış!";
					} else {
						$strrating = "" . $totalrating / $ratingcount;
						echo "(" . substr($strrating, 0, 4) . " / 10) ___ " . $ratingcount . " Oy";
					}
					echo "<br /></b>";
					// 1. durum : kullanici giris yapmamis
					if(!isset($_SESSION['activeUser'])) {
				?>
						<p>
							Oy kullanabilmek için lütfen sisteme giriş yapın.
						</p>
				<?php
					} else {
						// 2. durum : kullanici oy kullanmamis
						if($userrating == 0) {
				?>
							<br />
							<form action="traderate.php" method="post">
								<select class = "starselect" name="ratelist">
									<option>1 (*)</option>
									<option>2 (**)</option>
									<option>3 (***)</option>
									<option>4 (****)</option>
									<option>5 (*****)</option>
									<option>6 (******)</option>
									<option>7 (*******)</option>
									<option>8 (********)</option>
									<option>9 (*********)</option>
									<option>10(**********)</option>
								</select> 
								&nbsp;&nbsp;
								<button name="submit" class = "btnrate"> Oyla </button>
								
							</form>
				<?php
						} else {
						// 3. durum : kullanici oy kullanmis
				?>
							<p>
								<br />
								Bu marka için <?php echo $userrating; ?> puan verdiniz.
							</p>
				<?php
						}
					}
				?>
			</div>
		</div>
	</div>