<?php
	session_start();
	
	// Baglanti kur
	$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
				
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	$kullaniciAdi = $_POST['userName'];
	$kullaniciParola = $_POST['password'];
	
	$sorgu = "SELECT * FROM user WHERE username = '". $kullaniciAdi . "'";
	$result = $conn->query($sorgu);

	if ($result->num_rows > 0) {
		$row = $result->fetch_assoc();
		$userid = $row["id"];
		$userpass = $row["pass"];
		$userrole = $row["role"];
		$userpictpath = $row["picturepath"];
				
		if($userpass == $kullaniciParola) {
			// oturum acildi. acanin id si
			$_SESSION['activeUser'] = $userid;
			$_SESSION['activeUserRole'] = $userrole;
			$_SESSION['activeUserName'] = $kullaniciAdi;
			$_SESSION['userpictpath'] = $userpictpath;
			unset($_SESSION['girisHata']);
			unset($_SESSION['guncelleHata']);
		} else {
			$_SESSION['girisHata'] = "Hatali parola! ";
		}
	} else {
		$_SESSION['girisHata'] = "Hatali kullanici adi!";
	}
			
	$conn->close();
	
	$git = "Location: " . $_SESSION['lastPage'];
	header($git);
?>