<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset= utf-8" />
<meta name="Author" content=" Alper SAHBAZ" />
<link rel="stylesheet" href="style/otokritik.css" />
<link rel="shortcut icon" href="image/araba.png" />
<title> </title>
</head>

<body>
<div class="container">
	<div id="header">
		<?php 
			session_start();
			$_SESSION['lastPage'] = "models.php?model=" . $_GET['model'];
			include('header.php');
		?>
	</div>

	<div class="dis_bolme">
		<div class="yuzdeLeft">
			<?php include('solpen.php'); ?>
		</div>
		
        <div class="yuzdeMid">
        	<?php
				$modelid = $_GET['model'];
				$_SESSION['secili_model'] = $modelid;
				// Baglanti kur
				$conn = mysqli_connect($_SESSION['servername'], $_SESSION['username'], $_SESSION['password'], $_SESSION['database_name']);
				
				if ($conn->connect_error) {
					die("Connection failed: " . $conn->connect_error);
				}
				$sorgu = "SELECT name FROM model WHERE id = ". $modelid;
				$result = $conn->query($sorgu);
		
				if ($result->num_rows > 0) {
					$row = $result->fetch_assoc();
					$modelname = $row["name"];
					
					// sayfanın title ini degistirmek icin
					echo "<script type=\"text/javascript\">
						document.title = \"$modelname\"
					</script>";
				} else {
					echo "0 results";
				}
				
				$conn->close();
				
				// ortadaki alan
				include('modelmid.php');
			?>
        </div>
		
        <div class="yuzdeRight">
			<?php include('sagpen.php'); ?>
		</div>

	</div>
    
    <div class="footer">
    	<?php include('footer.php'); ?>
    </div>
</div>
</body>
</html>