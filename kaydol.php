<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset= utf-8" />
<meta name="Author" content="Alper SAHBAZ" />
<title>Kaydol</title>
<link rel="stylesheet" href="style/otokritik.css" />
<link rel="shortcut icon" href="image/araba.png" />
</head>

<body>
<div class="container">

	<?php
		
		session_start();
		$_SESSION['lastPage'] = "index.php";
		$_SESSION['servername'] = "localhost";
		$_SESSION['username'] = "murat";
		$_SESSION['password'] = "123456";
		$_SESSION['database_name'] = "deneme";
	?>
	<div id="header">
		<?php include('header.php'); ?>
	</div>

	<div class="dis_bolme">
		<div class="yuzdeLeft">
			<?php include('solpen.php'); ?>
		</div>
		<div class="yuzdeMid">
			<?php
				if(isset($_SESSION['kaydolbasarili'])) {
					// ortadaki alan kayit bittikten sonra
			?>
					
					<div class = "kaydolbittimesaj">
						<center>TEBRİKLER</center><br/>
						OTOKRİTİK ailesine hoşgeldiniz. Sitemizin kullanıcı özelliklerini kullanabilmek için
						lütfen kullanıcı bilgilerinizle sisteme giriş yapın.
					</div>
					<div class = "kaydolsagok">
						<img src = "image/sagok.gif" class = "sagok"/>
					</div>
					
					
			<?php
					// kaydol geri donuslerini temizle
					unset($_SESSION['checkname']);
					unset($_SESSION['kaydolusername']);
					unset($_SESSION['kaydol1pass']);
					unset($_SESSION['kaydol2pass']);
					unset($_SESSION['kaydolname']);
					unset($_SESSION['kaydolsurname']);
					unset($_SESSION['kaydolmail']);
					
					unset($_SESSION['kaydolbasarili']);
				} else {
					// ortadaki alan kayit icin
					include('kaydolmid.php');
				}
			?>
        </div>
		<div class="yuzdeRight">
			<?php include('sagpen.php'); ?>
		</div>

	</div>
    
    <div class="footer">
    	<?php include('footer.php'); ?>
    </div>

</div>
</body>
</html>