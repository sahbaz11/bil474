<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset= utf-8" />
<meta name="Author" content="Alper SAHBAZ" />
<title>OTO KRİTİK</title>
<link rel="stylesheet" href="style/otokritik.css" />
<link rel="shortcut icon" href="image/araba.png" />
</head>

<body>
<div class="container">

	<?php
		
		session_start();
		$_SESSION['lastPage'] = "main.php";
		$_SESSION['servername'] = "127.0.0.1";
		$_SESSION['username'] = "murat";
		$_SESSION['password'] = "123456";
		$_SESSION['database_name'] = "deneme";
	?>
	<div id="header">
		<?php include('header.php'); ?>
	</div>

	<div class="dis_bolme">
		<div class="yuzdeLeft">
			<?php include('solpen.php'); ?>
		</div>
		<div class="yuzdeMid">
			<?php 
				// ortadaki alan
				include('mainmid.php');
			?>
        </div>
		<div class="yuzdeRight">
			<?php include('sagpen.php'); ?>
		</div>

	</div>
    
    <div class="footer">
    	<?php include('footer.php'); ?>
    </div>

</div>
</body>
</html>